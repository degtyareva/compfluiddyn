%%
% This script analyses the final state of the Gletsjer

clf;
[q,lineString] = system('tail -n 1 ../output/600op20000/output.dat');
[r,firstLineString] = system('head -n 1 ../output/600op20000/output.dat');

lineNum = str2num(lineString);
firstLineNum = str2num(firstLineString);

[maxT, sizeX] = size(lineNum);

H = lineNum - firstLineNum;

% 600/10000
% midpoint: x = 4825
% max(H) = 42.1115
%x = (1:sizeX) * 15000. / sizeX;
%root = 0.95*real(sqrt(-x + 4826)) - 1.95;

% 600/15000
% midpoint: x = 7233
% max(H) = 54.9904
%x = (1:sizeX) * 20000. / sizeX;
%root = 0.98*real(sqrt(-x + 7214)) - 1.6;

% 600/20000:
% midpoint: x = 9664
% max(H) = 66.57
x = (1:sizeX) * 30200. / sizeX;
root = 1.005*real(sqrt(-x + 9645)) - 1.5;

% 600/25000
% midpoint: x = 12080
% max(H) = 77.3035
%x = (1:sizeX) * 30200. / sizeX;
%root = 1.07*real(sqrt(-x + 12061)) - 4.5;

% 600/30000
% midpoint: x = 14500
% max(H) = 87.4558
%x = (1:sizeX) * 30200. / sizeX;
%root = 1.14*real(sqrt(-x + 14481)) - 7;

% 600/35000
% midpoint: x = 16900
% max(H) = 97.0974
%x = (1:sizeX) * 50200. / sizeX;
%root = 1.17*real(sqrt(-x + 16900)) - 9;

root(root < 0.) = 0.;

%lineNum(lineNum < 311.7) = -100;
rootplus = root + firstLineNum;


figure(1);
plot(x, firstLineNum, '-.k');
hold on;
plot(x(1:128), lineNum(1:128), '--k', 'LineWidth', 1);
plot(x(1:128), rootplus(1:128), '-k', 'LineWidth', 2);

xlim([0 12000])
xlabel('x (m)');
ylabel('height (m)');

hold off;
%ylim([-20 80])
