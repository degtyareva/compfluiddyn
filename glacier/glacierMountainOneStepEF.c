/*********************************************
 * 1D Glacier flow with EF method
 * Numerical Methods, Project 3
 * Copyright: Kaj-Ivar van der Wijst, 2014
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define SIZEX 400 
#define SIZETMAX 200000 

#define RHO 910.
#define MU 1.e-16
#define BETA sqrt(2.)*RHO*9.81

#define SCONST 3.

#define dt 12.                      /* Dit zou gebonden moeten zijn door de CFL condities */
#define dx 50200. / SIZEX           /* Alleen stabiel as kappa * dt / (2*dx*dx) <= 1 */

#define DEBUGX 1                   /* Wordt gebruikt om positie DEBUGX te printen */

/********
 * Size of Kangerdlugssuaq Glacier:
 * Length: 26.2km
 * Width: 6.3km
 * Maximum elevation: 625m
 * Coordinates: 68.6689,-33.0977
 ********/

double a (double h) {
    /* At the top of the glacier, we use an accumulation of 0.1 meter / day */
    double accumulationTop;
    accumulationTop = 0.1 / (1200); // 0.1 / (3600 * 24.);
    return (accumulationTop * 2. / 625.) * h - accumulationTop;
}


void calcH (double *h, double *b, double *temp, int index, int t) {
    /* This calculates the spatial derivative of temp and finally the new height profile */
    
    double deriv, derivB, H, secondderiv, secondderivabs, factorD, ah;
    double s;
    s = 3.;

    if (index == 0 || index == SIZEX-1) {
        h[index] = b[index];
    } else {

        /* First we need to calculate the first spatial derivative of h */
        deriv = (h[index+1] - h[index-1]) / (2*dx);
        derivB = (b[index+1] - b[index-1]) / (2*dx);
        /* and the second spatial derivative */
        secondderiv = (h[index+1] - 2*h[index] + h[index-1]) / (dx*dx);

        /* We need d(|dh/dx|)/dx: */
        if (deriv > 0)
            secondderivabs = secondderiv;
        else
            secondderivabs = -secondderiv;

        /* The ice height is given by: */
        H = h[index] - b[index];

        /* For the constant factorD, we use a fixed value, instaed of the wrong values given in the exercise */
        factorD = 0.000022765;

        /* At last, we calculate the new value for h: */
        h[index] += dt * factorD * (
                        pow(H, s+1.) * deriv * (s-1.)*pow(secondderivabs, s-2.) + 
                        pow(fabs(deriv), s-1.) * deriv * (s+1.) * pow(deriv - derivB, s) + 
                        pow(fabs(deriv), s-1.) * pow(H, s+1.) * secondderiv
                    ) + dt * a(h[index]);

        if (h[index] < b[index]) h[index] = b[index];

    }

}    



void kernel (double *h, double *b, double *temp, int t) {
    int i;

    /* Calculate the new height profile */
    for (i = 0; i < SIZEX; i++) calcH(h, b, temp, i, t);


}




int main (int argc, char argv[]) {
    
    /* Initialize variables */
    double *h, *b, *temp;           /* Array voor totale hoogte (h), grondhoogte (b) en een tijdelijke array (temp) */ 
    int numBytes;                   /* Size of the temperature array */
    int i,t;                        /* Indices for loops */
    FILE *output, *initCond;


    /* Allocate memory */
    numBytes = SIZEX * sizeof(double);      /* We save an array with SIZEX elements */
    h = (double *) malloc(numBytes);      
    b = (double *) malloc(numBytes);      
    temp = (double *) malloc(numBytes);      

    initCond = fopen("output/initCond.dat", "w");

    /* Initial conditions */
    for (i = 0; i < SIZEX; i++) {
        h[i] = -1.4713e-7 * (i*dx) * (i*dx) - 0.02 * (i*dx) + 625;
        //b[i] = 9.979e-7 * (i*dx) * (i*dx) - 0.05 * (i*dx) + 625;
        b[i] = 625. - 4*9.979e-7 * (i*dx - 25000) * (i*dx - 25000.);
        if (b[i] < 0.) b[i] = 0.;
        if (h[i] < b[i]) h[i] = b[i];
        h[i] = b[i];
        fprintf(initCond, "%e ", b[i]);
    }


    /* Initialize output file */
    output = fopen("output/outputGlacier1D_EF.dat", "w");
    
    for (t = 0; t < SIZETMAX; t++) {
        if (t % 50 == 1) {
            for (i = 0; i < SIZEX; i++) fprintf(output,"%e ",h[i]);
            fprintf(output,"\n");
        }
        kernel(h, b, temp, t);
    }

    return 0;

}
