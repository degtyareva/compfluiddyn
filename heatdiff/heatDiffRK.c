/*********************************************
 * Heat diffusion in a rod with Runge-Kutta 4 method
 * Numerical Methods, Project 3
 * Copyright: Kaj-Ivar van der Wijst, 2014
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define SIZEX 2001
#define SIZETMAX 1000
#define K 80.                       /* Heat conductivity coefficient */
#define RHO 7874.                   /* Density of solid iron */
#define CP 449.                     /* Heat capacity */
#define T0 300.

#define kappa K / (RHO*CP)          /* Thermal diffusion coefficient */
#define dt 0.01                     /* Dit zou gebonden moeten zijn door de CFL condities */
#define dx 0.001                    /* Alleen stabiel as kappa * dt / (2*dx*dx) <= 1 */

void calcK (double *in, double *out, int index) {
    /* Calculates the second derivative of the array 'in' */
    double tCurr, tXmin, tXplus;

    tCurr = in[index];

    /* Boundary conditions */
    if (index == 0) {
        tXmin = T0;
        tXplus = in[index+1];
    } else if (index == SIZEX-1) {
        tXmin = in[index-1];
        tXplus = T0;
    } else {
        tXmin = in[index-1];
        tXplus = in[index+1];
    }

    out[index] = (kappa / (dx * dx)) * (tXplus - 2*tCurr + tXmin);
}

void kernel (double *tIn, double *tOut, double *k1, double *k2, double *k3, double *k4, double *temp) {
    int i;

    /* The Runge-Kutta 4 method requires 4 steps: */

    /* First calculate k1 */
    for (i = 0; i < SIZEX; i++) {
        calcK(tIn, k1, i);
        temp[i] = tIn[i] + dt * k1[i] / 2.;
    }

    /* Then calculate k2 */
    for (i = 0; i < SIZEX; i++) {
        calcK(temp, k2, i);
        temp[i] = tIn[i] + dt * k2[i] / 2.;
    }
        
    /* Then calculate k3 */
    for (i = 0; i < SIZEX; i++) {
        calcK(temp, k3, i);
        temp[i] = tIn[i] + dt * k3[i];
    }

    /* Then finally calculate k4 */
    for (i = 0; i < SIZEX; i++) {
        calcK(temp, k4, i);
    }

    /* Now, by combining k1, k2, k3 and k4 together,
     * we can calculate the new temperature: */
    
    for (i = 0; i < SIZEX; i++) {
        tOut[i] = tIn[i] + dt * (k1[i] + 2. * k2[i] + 2. * k3[i] + k4[i]) / 6.;
    }
    
    /* Copy temperatures back */
    for (i = 0; i < SIZEX; i++)
        tIn[i] = tOut[i];

}




int main (int argc, char argv[]) {
    
    /* Initialize variables */
    double *tIn, *tOut;      /* Array with temperatures at one moment */
    double *k1, *k2, *k3, *k4;
    double tExt;
    int numBytes;   /* Size of the temperature array */
    int i,t;        /* Indices for loops */
    FILE *output;

    double *temp;
    temp = (double *) malloc(SIZEX * sizeof(double)); 

    /* Allocate memory */
    numBytes = SIZEX * sizeof(double);      /* We save an array with SIZEX elements */
    tIn = (double *) malloc(numBytes);      /* Every element of T contains the temperature
                                               at position x */
    tOut = (double *) malloc(numBytes);
    k1 = (double *) malloc(numBytes);
    k2 = (double *) malloc(numBytes);
    k3 = (double *) malloc(numBytes);
    k4 = (double *) malloc(numBytes);

    /* Initial conditions */
    for (i = 0; i < SIZEX; i++) tIn[i] = T0;
    /* We know that Q = c_p * rho * T */
    tExt = T0 + 1500.;
    tIn[1000] = tExt;

    /* Initialize output file */
    output = fopen("output/outputT_RK.dat", "w");
    
    for (t = 0; t < SIZETMAX; t++) {
        kernel(tIn, tOut, k1, k2, k3, k4, temp);
        for (i = 0; i < SIZEX; i++) fprintf(output,"%f ",tIn[i]);
        fprintf(output,"\n");
    }

    return 0;

}
