%%
% This script plots the variation of the temperature
% of a rod after one end being put in a fire

%clear;
%data = load('../output/outputT_implicit.dat');
%data2 = load('../output/outputT_AB.dat');

dt = 0.01;
dx = 0.001;

[maxT, sizeX] = size(data);
pause on;
x = (1:sizeX)*dx;

kappa = 80. / (7874.*449.);

figure(1);
T0 = 300. + (1500.*dx) * exp(-(x-(sizeX/2+0.5)*dx).^2 / (4*kappa*dt*90)) / sqrt(4*pi*kappa*dt*90);
T1 = 300. + (1500.*dx) * exp(-(x-(sizeX/2+0.5)*dx).^2 / (4*kappa*dt*300)) / sqrt(4*pi*kappa*dt*300);
T2 = 300. + (1500.*dx) * exp(-(x-(sizeX/2+0.5)*dx).^2 / (4*kappa*dt*600)) / sqrt(4*pi*kappa*dt*600);
T3 = 300. + (1500.*dx) * exp(-(x-(sizeX/2+0.5)*dx).^2 / (4*kappa*dt*900)) / sqrt(4*pi*kappa*dt*900);
plot(x, T0, '-k', 'LineWidth', 2);
hold on;
plot(x, T1, '-.k', 'LineWidth', 2);
plot(x, T2, '--k', 'LineWidth', 2);
plot(x, T3, ':k', 'LineWidth', 4);
xlim([0.94 1.06]);
hold off


%figure(3);

%for i = 1:5:maxT-1
%    subplot(3,1,1);
%    plot(x, data(i,:), x, data2(i,:))
%    xlim([0.9 1.1]);

%    subplot(3,1,2);
%    t = dt*i;
%    T = 300. + (1500.*dx) * exp(-(x-(sizeX/2+0.5)*dx).^2 / (4*kappa*t)) / sqrt(4*pi*kappa*t);
%    plot(x,T);
%    xlim([0.9 1.1]);
%    maxTnum = max(data(i,:));
%    maxTexact = max(T);
%    relerror = abs((maxTnum - maxTexact) / maxTnum);
%    abserror = abs(maxTnum - maxTexact);
%    title(['Timestep ' num2str(i) ', max reldiff: ' num2str(relerror) ', max absdiff: ' num2str(abserror)]);
%    
%    subplot(3,1,3);
%    plot(data(i,:) - data2(i,:));
%    title(['Max difference: ' num2str(max(abs(data(i,:)-data2(i,:))))]);
%
%    drawnow
%end
