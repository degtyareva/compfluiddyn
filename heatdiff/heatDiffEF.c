/*********************************************
 * Heat diffusion in a rod with EF method
 * Numerical Methods, Project 3
 * Copyright: Kaj-Ivar van der Wijst, 2014
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define SIZEX 2001
#define SIZETMAX 1000
#define K 80.                       /* Heat conductivity coefficient */
#define RHO 7874.                   /* Density of solid iron */
#define CP 449.                     /* Heat capacity */
#define T0 300.

#define kappa K / (RHO*CP)          /* Thermal diffusion coefficient */
#define dt 0.01                     /* Dit zou gebonden moeten zijn door de CFL condities */
#define dx 0.001                    /* Alleen stabiel as kappa * dt / (2*dx*dx) <= 1 */

void calcT (double *tIn, double *tOut, int index) {
    /* Calculates the temperature at position x=index,
     * and saving it to the new array tOut */
    double tCurr, tXmin, tXplus;

    tCurr = tIn[index];

    /* Boundary conditions */
    if (index == 0) {
        tXmin = T0;
        tXplus = tIn[index+1];
    } else if (index == SIZEX-1) {
        tXmin = tIn[index-1];
        tXplus = T0;
    } else {
        tXmin = tIn[index-1];
        tXplus = tIn[index+1];
    }

    tOut[index] = tCurr + (kappa * dt / (dx * dx)) * (tXplus - 2*tCurr + tXmin);
}

void kernel (double *tIn, double *tOut) {
    int i;
    /* Calculate new temperatures */
    for (i = 0; i < SIZEX; i++)
        calcT(tIn, tOut, i);
    
    /* Copy temperatures back */
    for (i = 0; i < SIZEX; i++)
        tIn[i] = tOut[i];

}




int main (int argc, char argv[]) {
    
    /* Initialize variables */
    double *tIn, *tOut;      /* Array with temperatures at one moment */
    double tExt;
    int numBytes;   /* Size of the temperature array */
    int i,t;        /* Indices for loops */
    FILE *output;


    /* Allocate memory */
    numBytes = SIZEX * sizeof(double);      /* We save an array with SIZEX elements */
    tIn = (double *) malloc(numBytes);      /* Every element of T contains the temperature
                                               at position x */
    tOut = (double *) malloc(numBytes);

    /* Initial conditions */
    for (i = 0; i < SIZEX; i++) tIn[i] = T0;
    /* We know that Q = c_p * rho * T */
    tExt = T0 + 1500.;
    tIn[1000] = tExt;

    /* Initialize output file */
    output = fopen("output/outputT_EF.dat", "w");
    
    for (t = 0; t < SIZETMAX; t++) {
        kernel(tIn, tOut);
        for (i = 0; i < SIZEX; i++) fprintf(output,"%f ",tIn[i]);
        fprintf(output,"\n");
    }

    return 0;

}
