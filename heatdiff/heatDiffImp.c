/********************************************
 * Heat diffusion in a rod with implicit trapezoidal method
 * Numerical Methods, Project 3
 * Copyright: Kaj-Ivar van der Wijst, 2014
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define SIZEX 2001
#define SIZETMAX 1000
#define K 80.                       /* Heat conductivity coefficient */
#define RHO 7874.                   /* Density of solid iron */
#define CP 449.                     /* Heat capacity */
#define T0 300.

#define kappa K / (RHO*CP)          /* Thermal diffusion coefficient */
#define dt 0.01                     /* Dit zou gebonden moeten zijn door de CFL condities */
#define dx 0.001                    /* Alleen stabiel as kappa * dt / (2*dx*dx) <= 1 */


void kernel (double *tIn, double *tOut, double *bvector, double *subdiag, double *diag, double *supdiag) {
    int i;
    double k, m;

    k = 0.5 * kappa * dt / (dx*dx);

    /* Create vector b */
    for (i = 0; i < SIZEX; i++) {
        switch (i) {
            case 0: bvector[i] = T0; break;
            case SIZEX-1: bvector[i] = T0; break;
            default:
                bvector[i] = k * tIn[i-1] + (1-2*k) * tIn[i] + k * tIn[i+1];
        }
    }

    /* Create the sub diagonal, diagonal and sup diagonal of the matrix A */
    for (i = 0; i < SIZEX; i++) {
        switch (i) {
            case 0:
                diag[i] = 1;
                supdiag[i] = 0;
                break;
            case SIZEX-1:
                subdiag[i] = 0;
                diag[i] = 1;
                break;
            default :
                subdiag[i] = -k;
                diag[i] = 1+2*k;
                supdiag[i] = -k;
                break;
        }
    }

    /* Solve tridiagonal matrix */
    for (i = 1; i < SIZEX; i++) {
        m = subdiag[i] / diag[i-1];
        diag[i] -= m * supdiag[i-1];
        bvector[i] -= m*bvector[i-1];
    }
    
    tOut[SIZEX-1] = bvector[SIZEX-1] / diag[SIZEX - 1];

    for (i = SIZEX - 2; i >= 0; i--) {
        tOut[i] = (bvector[i] - supdiag[i] * tOut[i+1]) / diag[i];
    }
    
    /* Copy temperatures back */
    for (i = 0; i < SIZEX; i++)
        tIn[i] = tOut[i];

}




int main (int argc, char argv[]) {
    
    /* Initialize variables */
    double *tIn, *tOut;      /* Array with temperatures at one moment */
    double *bvector;         /* Vector in the equation Ax=b */
    double *subdiag, *diag, *supdiag;
    double tExt;
    int numBytes;   /* Size of the temperature array */
    int i,t;        /* Indices for loops */
    FILE *output;


    /* Allocate memory */
    numBytes = SIZEX * sizeof(double);      /* We save an array with SIZEX elements */
    tIn = (double *) malloc(numBytes);      /* Every element of T contains the temperature
                                               at position x */
    tOut = (double *) malloc(numBytes);
    bvector = (double *) malloc(numBytes);
    subdiag = (double *) malloc(numBytes);
    diag = (double *) malloc(numBytes);
    supdiag = (double *) malloc(numBytes);

    /* Initial conditions */
    for (i = 0; i < SIZEX; i++) tIn[i] = T0;
    /* We know that Q = c_p * rho * T */
    tExt = T0 + 1500.;
    tIn[1000] = tExt;

    /* Initialize output file */
    output = fopen("output/outputT_implicit.dat", "w");
    
    for (t = 0; t < SIZETMAX; t++) {
        kernel(tIn, tOut, bvector, subdiag, diag, supdiag);
        for (i = 0; i < SIZEX; i++) fprintf(output,"%f ",tIn[i]);
        fprintf(output,"\n");
    }

    return 0;

}
